#!/usr/bin/env ruby

require 'open3'
require 'date'
require 'uri'

STDOUT.sync = true
STDERR.sync = true

GOOGLE_APPLICATION_CREDENTIALS = ENV.fetch('GOOGLE_APPLICATION_CREDENTIALS')
DATABASE_URLS = ENV.fetch('DATABASE_URLS').split(',').map {|s| URI.parse(s) }
BUCKET_URL     = ENV.fetch('BUCKET_URL')
PG_DUMP_FLAGS  = %w[
  --no-owner --no-privileges --no-publications --no-security-labels --no-subscriptions
  --no-synchronized-snapshots --no-tablespaces --no-unlogged-table-data
].freeze

# setup boto
File.open('/root/.boto', 'w') do |file|
  file.puts <<~TEXT
    [Credentials]
    gs_service_key_file = #{GOOGLE_APPLICATION_CREDENTIALS}

    [GSUtil]
    default_api_version = 2
  TEXT
end

def log(msg)
  STDOUT.puts "#{Time.now.strftime("%Y-%m-%d %H:%M:%S")} #{msg}"
end

def fatal!(reason=nil)
  log('    Aborting!')
  STDERR.puts if reason
  STDERR.puts(reason) if reason
  exit(1)
end

loop do
  today = Date.today.strftime('%Y/%m/%d')
  local = 'dump.sql.gz'

  DATABASE_URLS.each do |url|
    name   = url.path.sub('/', '')
    target = [BUCKET_URL, today, "#{name}.sql.gz"].join('/')
    log "[+] Backup #{target}"

    output, status = Open3.capture2e('gsutil', 'stat', target)
    if status.success?
      log '    Skip'
      next
    elsif !output.include?('No URLs matched')
      fatal!(output)
    end

    pipe = Open3.pipeline(
      ['pg_dump'] + PG_DUMP_FLAGS + [url.to_s],
      ['gzip', '-c'],
      out: local,
      err: STDERR,
    )
    fatal! unless pipe.all?(&:success?)

    ouput, status = Open3.capture2e('gsutil', 'cp', local, target)
    unless status.success?
      fatal!(output)
    end
  end

  log "[=] Wait for next run"
  sleep(600 + rand(120))
end
