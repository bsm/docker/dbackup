OWNER=blacksquaremedia
NAME=dbackup
REPO=$(OWNER)/$(NAME)
BUILDS=$(patsubst %/Dockerfile,build/%,$(sort $(wildcard */Dockerfile)))
PUSHES=$(patsubst %/Dockerfile,push/%,$(sort $(wildcard */Dockerfile)))

build: pull $(BUILDS)
push: $(PUSHES)

.PHONY: build push

# ---------------------------------------------------------------------

DEPS=$(shell grep -h 'FROM' ./*/Dockerfile | grep -v blacksquaremedia | sed -e 's/FROM //')

pull:
	for dep in $(DEPS); do docker pull $$dep; done

build/%: %
	docker build -t $(REPO):$< $</

push/%: % build/%
	docker push $(REPO):$<
